function ShipEffect( scene, camera ) {

    const rings = new THREE.Mesh(
        new THREE.PlaneGeometry(1, 1), new THREE.ShaderMaterial({
        vertexShader: `
            varying vec2 vUv;
            void main() {
                vUv = uv;
                gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0);
            }
        `,
        fragmentShader: `
            uniform float time;
            uniform float opacity;
            varying vec2 vUv;
            void main() {
                vec2 xy = vUv * 2.0 - 1.0;
                float r = length(xy);
                float f = fract(r * 3.0 - time) * opacity;
                float a = pow(1.1 * f, 3.0) * max(0.0, 1.0 - r);
                gl_FragColor = vec4(
                    mix(vec3(10.0, 28.0, 46.0) / 255.0, vec3(0.0, 1.0, 1.0), a),
                1.0);
            }
        `,
        uniforms: {
            time: { get value() { return (Date.now() % 2000) / 2000 }},
            opacity: { get value() { return rings.material.opacity }}
        }/*,
        transparent: true*/
    }));
    rings.material.opacity = 1;
    rings.position.y = -0.001;
    rings.rotation.x = -0.5 * Math.PI;
    scene.add(rings);

    const ship = new THREE.Group();
    const vessels = [];

    (new THREE.GLTFLoader()).load('ship.glb', function(result) {
        for(let i = 0; i < 3; i++) {
            const mesh = result.scene.getObjectByName('Ship' + (i + 1));

            mesh.scale.setScalar(1.7 * mesh.scale.x);
            whitify(mesh);

            if(i > 0) {
                mesh.parent = ship;
            } else {
                ship.add(mesh);
            }
            vessels.push(mesh);

            const mesh2 = mesh.clone(); mesh2.scale.setScalar(1.02);
            wireframify(mesh2, 0xffff);

            // dots were removed
            const nothing = new THREE.Group();
            nothing.visible = false;
            mesh.add( nothing );

            mesh.add( mesh2 );
        }
    });
    scene.add(ship);

    const sattelite = new THREE.Mesh(
        new THREE.PlaneGeometry(0.42, 0.42),
        new THREE.MeshBasicMaterial({
            map: new THREE.TextureLoader().load('images/satellite.png'),
            transparent: true, depthTest: false, depthWrite: false
        })
    );
    sattelite.quaternion.copy(camera.quaternion);
    sattelite.position.set(-0.3, 0.5, 0);
    scene.add(sattelite);




    const boxParticle = new THREE.TextureLoader().load('images/particle.png');

    const floorPlane = createPlane( 1 );
    scene.add(floorPlane);

    const msx /* mesh.scale.x */ = 0.00014673851474;
    const boxes = [], box0 = createBox( /*msx * 3246 / 12*/ 0.02 );
    const boxMats = [
        box0.material.clone(), box0.material.clone(), box0.material.clone()
    ]
    for(let i = 0; i < 77/*576*/; i++) {
        const b = box0.clone();
        const r = Math.random();
        b.material = boxMats[0];
        if( r < 0.333 ) b.material = boxMats[1];
        if( r > 0.666 ) b.material = boxMats[2];
        b.rot = (function() {
            const x = 2 * Math.random(), y = Math.random(), z = Math.random(), w = 0.5 + Math.random();
            return function(t) {
                this.rotation.set(
                    x + t, y + w * t, z
                );
            };
        }) ();
        b.rot(0);
        b.rnd = [
            Math.random(),
            Math.random(),
            Math.random()
        ];
        boxes.push( b );
        scene.add( b );
    }



    // helper functions
    function whitify (mesh) {
        mesh.material = new THREE.ShaderMaterial({
            vertexShader: `varying vec2 vUV; void main () {
                vUV = uv;
                gl_Position = projectionMatrix * modelViewMatrix * vec4 ( position, 1.0 );
            }`,
            fragmentShader: `uniform sampler2D map; varying vec2 vUV; void main () {
                gl_FragColor = texture2D ( map, vUV );
            }`,
            uniforms: {
                map: { value: mesh.material.map }
            }
        });
        mesh.material.dissolve = 0;
    }

    function wireframify (mesh, color) {
        var uv2 = mesh.geometry.getAttribute ('uv2');
        if(!uv2) {
            uv2 = new THREE.BufferAttribute(new Float32Array(2 * mesh.geometry.getAttribute ('position').array.length / 3), 2);
            mesh.geometry.setAttribute('uv2', uv2);
        }

        var values = [0,0, 0,1, 1,0];
        for (var i = 0; i < uv2.array.length; i++) uv2.array[i] = values[i % 6];

        mesh.material = new THREE.ShaderMaterial({
            vertexShader: `attribute vec2 uv2; varying vec2 vUV; varying vec2 vUV2; varying vec3 vP; void main () {
                vUV = uv; vUV2 = uv2; vP = position;
                gl_Position = projectionMatrix * modelViewMatrix * vec4 ( position, 1.0 );
            }`,
            // bounding box is:
            // x -3245.4  ... +3245.4 | 12 * 2
            // y    -0.06 ... +1559.2 | 6
            // z  -525.9  ...  +525.9 | 2 * 2
            //                        | = 576 boxes
            fragmentShader: `float hash(float p) {
                p = fract(p * .1031);
                p *= p + 33.33;
                p *= p + p;
                return fract(p);
            }
            varying vec2 vUV; varying vec2 vUV2; varying vec3 vP;
            uniform vec3 color; uniform float opacity; uniform float dissolve;
            uniform sampler2D map; uniform sampler2D noise; void main () {
                // wireframe output
                vec4 output2 = vec4(color,
                    opacity * max(0.0, min(1.0, 2.0 - 4.0 * min (vUV2.x, min (vUV2.y, 1.0 - (vUV2.x + vUV2.y))) / length(fwidth(vUV2)) ))
                );

                vec3 vP2 = floor(
                     vP / vec3( 270.5, 260.0, 263.0 ) + vec3( 12.0, 0.00023, 2.0 )
                ); // 0 ... 23, 0 ... 5, 0 ... 3

                vP2.y = 5.0 - vP2.y;

                float iP = vP2.x + ( vP2.z + vP2.y * 4.0 ) * 24.0;

                if( hash( 0.5 - iP / 575.0 ) < dissolve ) {
                    discard;
                }

                gl_FragColor = mix( output2,
                    vec4(1.0, 1.0, 1.0, 1.0 - output2.b),
                0.25 );
            }`,
            transparent: true,
            uniforms: {
                map: { value: mesh.material.map
                    || new THREE.TextureLoader().load( 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7' )
                },
                noise: { value: new THREE.TextureLoader().load( './images/perlin.jpg' ) },
                color: { value: new THREE.Color(color) },
                opacity: { get value() { return mesh.material.opacity } },
                dissolve: { get value() { return mesh.material.dissolve } }
            }
        });
        mesh.material.dissolve = 0;
        mesh.material.extensions.derivatives = true;
        mesh.material.morphTargets = true;

        return mesh;
    }

    function createBox( size ) {
        const min = { x: -size / 2, y: -size / 2, z: -size / 2 };
        const max = { x:  size / 2, y:  size / 2, z:  size / 2 };
        const makeGeometry = function( indices, positions ) {
            const geometry = new THREE.BufferGeometry();
            geometry.setIndex( new THREE.BufferAttribute( indices, 1 ) );
            geometry.setAttribute( 'position', new THREE.BufferAttribute( positions, 3 ) );
            return geometry;
        }
        // cube geometry
        const indices = new Uint16Array( [ 0, 1, 1, 2, 2, 3, 3, 0, 4, 5, 5, 6, 6, 7, 7, 4, 0, 4, 1, 5, 2, 6, 3, 7 ] );
        const array = new Array( 8 * 3 );
        array[ 0 ] = max.x; array[ 1 ] = max.y; array[ 2 ] = max.z;
        array[ 3 ] = min.x; array[ 4 ] = max.y; array[ 5 ] = max.z;
        array[ 6 ] = min.x; array[ 7 ] = min.y; array[ 8 ] = max.z;
        array[ 9 ] = max.x; array[ 10 ] = min.y; array[ 11 ] = max.z;
        array[ 12 ] = max.x; array[ 13 ] = max.y; array[ 14 ] = min.z;
        array[ 15 ] = min.x; array[ 16 ] = max.y; array[ 17 ] = min.z;
        array[ 18 ] = min.x; array[ 19 ] = min.y; array[ 20 ] = min.z;
        array[ 21 ] = max.x; array[ 22 ] = min.y; array[ 23 ] = min.z;
        const positions = new Float32Array( array );
        const colors = new Float32Array( array.map( (e, i) => (i % 3 > 0) ? 1 : 0 ) );
        // sphere geometry
        const indices2 = new Uint16Array( [ 11, 25, 2, 11, 2, 13, 13, 25, 19, 24, 6, 19, 6, 18, 18, 24, 16, 23, 5, 16, 5, 12, 12, 23, 18, 22, 6, 14, 14, 22, 19, 21, 7, 19, 7, 15, 15, 21, 9, 20, 3, 9, 3, 15, 15, 20, 17, 20, 7, 17, 4, 10, 10, 20, 4, 17, 0, 10, 0, 9, 13, 21, 3, 13, 2, 14, 14, 21, 11, 22, 1, 12, 12, 22, 1, 11, 5, 18, 8, 23, 1, 8, 10, 23, 0, 8, 4, 16, 16, 24, 17, 24, 9, 25, 8, 25 ] );
        const positions2 = new Float32Array( [
            0.577, -0.577, -0.577, 
            0.577, -0.577, 0.577, 
            -0.577, -0.577, 0.577, 
            -0.577, -0.577, -0.577, 
            0.577, 0.577, -0.577, 
            0.577, 0.577, 0.577, 
            -0.577, 0.577, 0.577, 
            -0.577, 0.577, -0.577, 
            0.707, -0.707, 0, 
            0, -0.707, -0.707, 
            0.707, 0, -0.707, 
            0, -0.707, 0.707, 
            0.707, 0, 0.707, 
            -0.707, -0.707, 0, 
            -0.707, 0, 0.707, 
            -0.707, 0, -0.707, 
            0.707, 0.707, 0, 
            0, 0.707, -0.707, 
            0, 0.707, 0.707, 
            -0.707, 0.707, 0, 
            0, 0, -1, 
            -1, 0, 0, 
            0, 0, 1, 
            1, 0, 0, 
            0, 1, 0, 
            0, -1, 0
        ].map (n => n * size / 1.5 ) );
        const geometry1 = makeGeometry( indices, positions );
        const geometry2 = makeGeometry( indices2, positions2 );
        const geometry = new THREE.BufferGeometry();
        geometry.switchTo = function( i ) {
            if( i ) {
                geometry.setIndex( geometry2.index );
                geometry.setAttribute( 'position', geometry2.attributes.position );
            } else {
                geometry.setIndex( geometry1.index );
                geometry.setAttribute( 'position', geometry1.attributes.position );
            }
        };
        geometry.switchTo( 0 );
        const ls = new THREE.LineSegments( geometry, new THREE.LineBasicMaterial( { color: 0xFFFF, toneMapped: false/*, transparent: true*/ } ) );
        const sprite = new THREE.Sprite( new THREE.SpriteMaterial( {
            map: boxParticle,
            transparent: true, opacity: 0.3, depthWrite: false,
            //blending: THREE.AdditiveBlending
        } ) );
        sprite.scale.setScalar( 0.14 );
        ls.add( sprite );
        ls.visible = false;
        return ls;
    }

    function createPlane( size ) {
        const px = [];
        const v = function( i, j ) {
            return {
                x: 0.1 * size * i, y: 0, z: 0.1 * size * j
            }
        };
        for(var i = -10; i < 10; i++) {
            for(var j = -5; j < 5; j++) {
                // a -- b
                // |    |
                // c -- d
                let a = v(i, j);
                if( i < 9 ) {
                    let b = v(i + 1, j);
                    px.push(a.x, a.y, a.z, b.x, b.y, b.z);
                    if( j < 4 ) {
                        let d = v(i + 1, j + 1);
                        px.push(d.x, d.y, d.z, b.x, b.y, b.z);
                    }
                }
                if( j < 4 ) {
                    let c = v(i, j + 1);
                    px.push(a.x, a.y, a.z, c.x, c.y, c.z);
                    if( i < 9 ) {
                        let d = v(i + 1, j + 1);
                        px.push(d.x, d.y, d.z, c.x, c.y, c.z);
                    }
                }
            }
        }

        // subdivide x4
        const px4 = [];
        for(var i = 0; i < px.length; i += 6) {
            const ax = px[i], ay = px[i+1], az = px[i+2], bx = px[i+3], by = px[i+4], bz = px[i+5];
            for(var j = 0; j < 4; j++) {
                const f = j / 4, g = (j + 1) / 4;
                px4.push(
                    ax * (1 - f) + bx * f, ay * (1 - f) + by * f, az * (1 - f) + bz * f,
                    ax * (1 - g) + bx * g, ay * (1 - g) + by * g, az * (1 - g) + bz * g
                )
            }
        }
        
        const geometry = new THREE.BufferGeometry();
        geometry.setAttribute( 'position', new THREE.BufferAttribute( new Float32Array(px4), 3 ) );
        const material = new THREE.LineBasicMaterial( { color: 0xFFFF, toneMapped: false, transparent: true } );
        material.onBeforeCompile = function( hack ) {
            hack.vertexShader = 'varying float vH; uniform float height; uniform float model;\n' + hack.vertexShader
                .replace(
                    '#include <begin_vertex>',
                    `float rightArgX = max( 0.0, (position.x - 0.3) * 6.0 );
                    float rightArgZ = position.z * 1.6;
                    float leftArgX = max( 0.0, (- position.x - 0.1) * 5.0 );
                    float leftArgZ = position.z * 6.0;
                    vH = height * (
                        ( 1.0 - cos( rightArgX ) ) * cos( rightArgZ + model ) +
                        0.25 * ( 1.0 - cos( leftArgX ) ) * ( 1.0 - cos( leftArgZ + 2.0 * model ) )
                    );
                    vec3 transformed = vec3(
                        position.x,
                        position.y + 0.2 * vH,
                        position.z
                    );`
                );
            hack.fragmentShader = `varying float vH; uniform float opa; uniform float height; void main() {
                vec3 c0 = vec3(0.0, 1.0, 1.0);
                vec3 c1 = //vec3(0.79, 0.58, 0.14);
                          vec3(0.894, 0.565, 0.333);
                vec3 c2 = vec3(0.5, 0.0, 0.0);
                float a = opa - 0.3 * height + 0.2 * vH;
                if (vH < 1.0) {
                    gl_FragColor = vec4( mix(c0, c1, vH), a );
                } else {
                    gl_FragColor = vec4( mix(c1, c2, min(1.0, (vH - 1.0) / 0.25)), a );
                }
            }`;
            hack.uniforms.height = { get value() { return material.height }};
            hack.uniforms.opa = { get value() { return material.opacity }};

            hack.uniforms.model = { get value() { return ShipEffect.model }};
        };
        material.height = 0; material.opacity = 0;

        const plane = new THREE.LineSegments( geometry, material );
        plane.add( createPath() );

        return plane;
    }

    function createPath() {
        const vertices = [];
        for( let i = 4; i < 10; i++) {
            vertices.push(-0.1 * i, 0.04, 0);
        }
        const geometry = new THREE.BufferGeometry();
        geometry.setAttribute( 'position', new THREE.Float32BufferAttribute( vertices, 3 ) );

        const material = new THREE.PointsMaterial( { map: boxParticle, size: 0.04, transparent: true, opacity: 0 } );
        material.onBeforeCompile = function( hack ) {
            hack.vertexShader = 'uniform float time; uniform float model;\n' + hack.vertexShader
                .replace(
                    '#include <morphtarget_vertex>',
                    'transformed.x -= 0.1 * time; transformed.y -= 0.1 * cos(transformed.x * 4.0); transformed.z += 0.2 * sin(transformed.x * ( model - 0.6 ));'
                )
                ;
            hack.uniforms.time = { get value() { return (Date.now() % 2000) / 2000 }};
            hack.uniforms.model = { get value() { return ShipEffect.model }};
        };

        return new THREE.Points( geometry, material );
    }

    const extraRotationY = [0, -0.5, 0.2];

    // return update function
    return function( shipRotationY, satelliteX, satelliteY,
        // 0...+Infinity
        whiteToWireframe,
        // 0...+Infinity
        ringTransparency,
        // 0...+Infinity
        wireframeToPoints,
        // 0...1
        shipOpacity,
        // 0...1
        floorPlaneOpacity,
        // 0...1
        floorPlaneHeight,
        // 0...1
        ship2OneMinusHeight
    ) {
        ship.rotation.y *= 0.9;
        ship.rotation.y += 0.1 * (shipRotationY + extraRotationY[ ShipEffect.vessel ]);

        sattelite.position.x *= 0.9;
        sattelite.position.x += 0.1 * satelliteX;
        sattelite.position.y *= 0.9;
        sattelite.position.y += 0.1 * satelliteY;
        sattelite.scale.setScalar( 1 + sattelite.position.x ); // grow

        const mesh = vessels[ShipEffect.vessel];
        if(mesh) {
            ship.children[0] = mesh;
            mesh.material.opacity = shipOpacity;

            // change from white to wireframe
            let du = whiteToWireframe;
            let d = Math.min( 1, du );
            /*
            mesh.material.dissolve *= 0.95;
            mesh.material.dissolve += 0.05 * d;
            */

            // hide the rings
            let a = Math.min( 1, ringTransparency );

            rings.material.opacity = Math.min(1, 2 - 2 * a );
            rings.scale.setScalar( 1 + a );

            // 2nd ship
            mesh.children[1].material.visible = (a > 0);
            mesh.children[1].material.dissolve = 1 - a;
            mesh.children[1].material.opacity = Math.min( a * 1.5, 1 );
            mesh.children[1].position.y = 3456 * ( 1 - ship2OneMinusHeight );

            // manage the floor and ship particles
            floorPlane.material.opacity = floorPlaneOpacity;
            floorPlane.material.height = floorPlaneHeight;

            floorPlane.children[0].material.opacity = floorPlane.material.height;
            sattelite.material.opacity = rings.material.opacity;

            let stage = 0;
            boxMats[1].color.setHex( 0xFFFF );
            boxMats[2].color.setHex( 0xFFFF );

            if( wireframeToPoints > 0 ) {
                stage = 1;
                boxMats[1].color.setHex( 0xE49055 );
                boxMats[2].color.setHex( 0xEEEEEE );

                du = wireframeToPoints;
                d = Math.min( 1, du );
            }

            box0.geometry.switchTo( stage );
            box0.children[0].material.visible = (stage < 1);

            for(let i = 0; i < boxes.length; i++) {
                if( i / ( boxes.length - 1 ) >= d ) {
                    boxes[i].position.setScalar( 0 );
                    if( stage ) {
                        boxes[i].position.copy( sattelite.position );
                    }
                    boxes[i].visible = (sattelite.material.opacity > 0.9);
                } else {
                    let f = Math.min( 1, du - i / ( boxes.length - 1 ) );
                    let x = boxes[i].position.x;
                    let y = boxes[i].position.y;
                    let z = boxes[i].position.z;
                    boxes[i].position.set(
                        6490 * ( boxes[i].rnd[0] - 0.5 ),
                          1500 * boxes[i].rnd[1],
                        1050 * ( boxes[i].rnd[2] - 0.5 )
                    ).multiplyScalar( stage ? 0.8 * msx : msx ).add( {
                        x: 0.012605335, y: 0.0000825, z: 0.0011833
                    } );
                    ship.localToWorld(
                        boxes[i].position
                    );
                    boxes[i].position.multiplyScalar( stage ? f : 1 - f ).addScaledVector(
                        sattelite.position, stage ? 1 - f : f
                    );
                    boxes[i].position.multiplyScalar( 0.1 );
                    boxes[i].position.x += 0.9 * x;
                    boxes[i].position.y += 0.9 * y;
                    boxes[i].position.z += 0.9 * z;
                    boxes[i].rot( 4.2 * f );
                    
                    boxes[i].visible = (f > 0.01) && (f < 0.99);
                }
            }
        }
    };
}

ShipEffect.vessel = 0
ShipEffect.model = 0
